# lava-lamp

Lava lamp in [Rust](https://www.rust-lang.org/) with [Piston](https://www.piston.rs/).

I started playing with [Alex Lugo's lava lamp](https://www.youtube.com/watch?v=6erSNPToziw) and added some command-line options with [structopt](https://crates.io/crates/structopt).

```
$ lava_lamp --help
lava_lamp 0.1.0
Lava lamp options

USAGE:
    lava_lamp [FLAGS] [OPTIONS]

FLAGS:
    -d, --debug      Activate debug mode
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --background-color <background-color>      Background color [default: 68dd13]
        --bubble-color <bubble-color>              Bubble color [default: ff6100]
        --bubble-radius-min <bubble-radius-min>    Minimum radius of bubbles [default: 5]
        --bubble-speed-min <bubble-speed-min>      Minimum speed of bubbles [default: 10]
        --bubbles-min <bubbles-min>                Minimum number of bubbles [default: 20]
        --bubbles-mod <bubbles-mod>                Modulus for number of bubbles [default: 25]
        --lamp-height <lamp-height>                Height of display [default: 800]
        --lamp-width <lamp-width>                  Width of display [default: 600]
```

Here is what it looks like on my machine, just running `lava_lamp` with all the defaults:

![screen shot](screen-shot.png)

