/// Programming a Lava Lamp in Rust (ft. Piston)
/// https://www.youtube.com/watch?v=6erSNPToziw
use piston_window::*;
use rand::prelude::random;
use regex::Regex;
use structopt::StructOpt;

/// Lava lamp options
#[derive(StructOpt, Debug)]
struct Opt {
    /// Background color
    #[structopt(long, parse(try_from_str = parse_color), default_value = "68dd13")]
    background_color: piston_window::types::Color,

    /// Bubble color
    #[structopt(long, parse(try_from_str = parse_color), default_value = "ff6100")]
    bubble_color: piston_window::types::Color,

    /// Minimum radius of bubbles
    #[structopt(long, default_value = "5")]
    bubble_radius_min: f64,

    /// Minimum speed of bubbles
    #[structopt(long, default_value = "10")]
    bubble_speed_min: f64,

    /// Minimum number of bubbles
    #[structopt(long, default_value = "20")]
    bubbles_min: u32,

    /// Modulus for number of bubbles
    #[structopt(long, default_value = "25")]
    bubbles_mod: u32,

    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Height of display
    #[structopt(long, default_value = "800")]
    lamp_height: f64,

    /// Width of display
    #[structopt(long, default_value = "600")]
    lamp_width: f64,
}

struct Bubble {
    x: f64,
    y: f64,
    r: f64,
    speed: f64,
}

impl Bubble {
    pub fn new(
        bubble_depth: Option<f64>,
        lamp_height: f64,
        lamp_width: f64,
        min_radius: f64,
        min_speed: f64,
    ) -> Bubble {
        let x = random::<f64>() * lamp_width;
        let r = min_radius + (random::<f64>() * (lamp_width / 8.0));
        let (y, speed) = match bubble_depth {
            // If we're given a depth, make a fixed bubble there.
            Some(depth) => (depth, 0.0),

            // Otherwise, make a bubble in a random place that moves
            // at a random speed.
            None => (
                random::<f64>() * (lamp_height + r),
                min_speed + (random::<f64>() * min_speed * 9.0),
            ),
        };
        Bubble { x, y, r, speed }
    }
}

fn get_bubbles(
    lamp_height: f64,
    lamp_width: f64,
    bubbles_min: u32,
    bubbles_mod: u32,
    bubble_radius_min: f64,
    bubble_speed_min: f64,
) -> Vec<Bubble> {
    let mut bubbles = Vec::new();
    let number_of_bubbles = bubbles_min + (random::<u32>() % bubbles_mod);
    for _ in 0..number_of_bubbles {
        // Bubbles at the top.
        bubbles.push(Bubble::new(
            Some(lamp_height),
            lamp_height,
            lamp_width,
            bubble_radius_min,
            bubble_speed_min,
        ));

        // Bubbles at the bottom.
        bubbles.push(Bubble::new(
            Some(0.0),
            lamp_height,
            lamp_width,
            bubble_radius_min,
            bubble_speed_min,
        ));

        // Moving bubbles.
        bubbles.push(Bubble::new(
            None,
            lamp_height,
            lamp_width,
            bubble_radius_min,
            bubble_speed_min,
        ));
    }
    bubbles
}

/// Parse hexadecimal digits (RGBA) into a color .
fn parse_color(src: &str) -> Result<piston_window::types::Color, &str> {
    let re = Regex::new(r"^[[:xdigit:]]{6,8}$").unwrap();

    if re.is_match(src) {
        Ok(color::hex(src))
    } else {
        Err("could not parse color")
    }
}

fn main() {
    let opt = Opt::from_args();

    // Only echo options if the debug flag was set.
    if opt.debug {
        println!("{:#?}", opt);
    }

    let mut bubbles: Vec<Bubble> = get_bubbles(
        opt.lamp_height,
        opt.lamp_width,
        opt.bubbles_min,
        opt.bubbles_mod,
        opt.bubble_radius_min,
        opt.bubble_speed_min,
    );

    let mut window: PistonWindow =
        WindowSettings::new("Lava Lamp", [opt.lamp_width, opt.lamp_height])
            .exit_on_esc(true)
            .build()
            .unwrap();

    let mut events = window.events;

    while let Some(e) = events.next(&mut window) {
        if e.render_args().is_some() {
            window.draw_2d(&e, |c, g, _| {
                clear(opt.background_color, g);
                for b in &bubbles {
                    ellipse(
                        opt.bubble_color,
                        [b.x - b.r, b.y - b.r, b.r * 2.0, b.r * 2.0],
                        c.transform,
                        g,
                    )
                }
            });
        }
        if let Some(u) = e.update_args() {
            for b in &mut bubbles {
                b.y -= b.speed * u.dt;
                if b.y + b.r <= 0.0 {
                    b.y = opt.lamp_height + b.r;
                }
            }
        }
    }
}
